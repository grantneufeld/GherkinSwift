# GherkinSwift

A code library for parsing Gherkin format feature files, and processing them with Swift.

⚠️ This is incomplete and doesn’t work yet.
Go through the code and find the “⚠️” comments to see where stuff still needs to be done.

## Documentation

* [Syntax supported by GherkinSwift](Docs/GherkinSyntax.md)
* [Using GherkinSwift](Docs/Usage.md)
* [Tips for writing good features](Docs/Tips.md)

## Similar or Related Projects

* [Cucumber](https://cucumber.io/)
* [SwiftGherkin](https://github.com/iainsmith/SwiftGherkin)
* [TABTestKit](https://github.com/theappbusiness/TABTestKit/)
* [Ploughman](https://github.com/kylef-archive/Ploughman)
* [XCFit](https://github.com/XCTEQ/XCFit)
* [XCTest-Gherkin](https://github.com/net-a-porter-mobile/XCTest-Gherkin)

## Legal

Copyright ©2021 Grant Neufeld.

Available for use under the terms of the [Do No Harm License](LICENSE.md).

Participants in, and contributors to, this project are subject to the [Code of Conduct](code_of_conduct.md).
