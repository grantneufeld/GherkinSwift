import Foundation

/// Handles reporting of messages (errors, warnings, failures).
public protocol MessageReporter {
    /// Report an error.
    /// - Parameters:
    ///   - content: the string of text that the error applies to
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    func error(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?)

    /// Report a warning.
    /// - Parameters:
    ///   - content: the string of text that the error applies to
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    func warning(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?)

    /// Report a test success.
    /// - Parameters:
    ///   - content: the string of text that the success applies to
    ///   - file: the FeatureFile the success occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    func success(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?)

    /// Report a test failure.
    /// - Parameters:
    ///   - content: the string of text that the error applies to 
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    func failure(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?)
}
