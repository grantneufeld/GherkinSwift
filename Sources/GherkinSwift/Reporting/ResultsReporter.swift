import Foundation

/// Stores messages for later retrieval.
public final class ResultsReporter: MessageReporter {
    private var messages: [ScenarioError] = []

    /// Report an error.
    /// - Parameters:
    ///   - content: the string of text that the error applies to 
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    public func error(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .error
        )
    }

    /// Report a warning.
    /// - Parameters:
    ///   - content: the string of text that the error applies to
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    public func warning(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .warning
        )
    }

    /// Report a test success.
    /// - Parameters:
    ///   - content: the string of text that the success applies to
    ///   - file: the FeatureFile the success occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    public func success(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .success
        )
    }

    /// Report a test failure.
    /// - Parameters:
    ///   - content: the string of text that the error applies to
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    public func failure(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .failure
        )
    }

    /// Add the message to the messages attribute.
    /// - Parameters:
    ///   - content: the string of text that the error applies to
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - example: if running as part of an example, the GherkinExample
    ///   - category: the ReportCategory for the message (e.g., .error, .warning, etc.)
    private func reportMessage(
        content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?, category: ReportCategory
    ) {
        let error = ScenarioError(
            content: content, file: file, lineNumber: lineNumber, example: example, category: category
        )
        messages.append(error)
    }

    // ⚠️ need to actually access/report the results hidden away in the messages attribute
}
