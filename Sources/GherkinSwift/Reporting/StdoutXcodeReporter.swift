import Foundation

/// Reports messages on stdout in a format Xcode parses for warnings/errors.
public struct StdoutXcodeReporter: MessageReporter {
    public init() {
    }

    /// Report an error.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    public func error(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .error
        )
    }

    /// Report a warning.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    public func warning(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .warning
        )
    }

    /// Report a test success.
    /// - Parameters:
    ///   - file: the FeatureFile the success occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the success applies to
    ///   - example: if running as part of an example, the GherkinExample
    public func success(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?) {
        // ⚠️ don’t report passing tests
    }

    /// Report a test failure.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    public func failure(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample? = nil) {
        reportMessage(
            content: content, file: file, lineNumber: lineNumber, example: example, category: .failure
        )
    }

    /// Print the message to stdout in a format Xcode can use.
    /// - Parameters:
    ///   - file: the FeatureFile the error occurred for
    ///   - lineNumber: the line number within the file
    ///   - content: the string of text that the error applies to
    ///   - example: if running as part of an example, the GherkinExample
    ///   - category: the ReportCategory for the message (e.g., .error, .warning, etc.)
    private func reportMessage(
        content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?, category: ReportCategory
    ) {
        let error = ScenarioError(
            content: content, file: file, lineNumber: lineNumber, example: example, category: category
        )
        print(error.xcode)
    }
}
