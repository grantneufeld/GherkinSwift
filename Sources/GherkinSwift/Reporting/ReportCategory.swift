/// There can be different types of message to report.
public enum ReportCategory: String {
    /// Default type of error.
    case error
    /// Just a warning; not a “stopping” error.
    case warning
    /// A test success.
    case success
    /// A test failure.
    case failure
}
