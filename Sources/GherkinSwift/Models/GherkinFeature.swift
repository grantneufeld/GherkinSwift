import Foundation

/// An individual feature with scenarios that can be tested.
public final class GherkinFeature: Collection, Equatable, Sequence {
    public typealias Element = GherkinScenario
    public typealias Index = Int

    /// The file the feature is in.
    let file: FeatureFile
    /// The line number that the feature starts on in the file.
    let lineNumber: Int
    var tags: Set<GherkinTag> = []
    var title: String
    var inOrder: String?
    var asA: String?
    var iWant: String?

    // MARK: - Collection and Sequence attributes

    private var scenarios: [GherkinScenario] = []
    public var count: Int {
        scenarios.count
    }
    public var startIndex: Int {
        0
    }
    public var endIndex: Int {
        count
    }
    /// Used when iterating over the collection of scenarios.
    private var nextIndex = 0

    // MARK: - Initialization

    init(file: FeatureFile, lineNumber: Int, title: String, tags: Set<GherkinTag>) {
        self.file = file
        self.lineNumber = lineNumber
        self.title = title
        self.tags = tags
    }

    // MARK: - Equatable

    public static func == (lhs: GherkinFeature, rhs: GherkinFeature) -> Bool {
        lhs.file == rhs.file &&
            lhs.lineNumber == rhs.lineNumber &&
            lhs.title == rhs.title &&
            lhs.tags == rhs.tags &&
            lhs.scenarios == rhs.scenarios
    }

    // MARK: - acts as a Collection/Sequence of Scenarios

    public subscript(position: Int) -> GherkinScenario {
        scenarios[position]
    }

    public func index(after inIndex: Int) -> Int {
        inIndex + 1
    }

    /// Get the next Scenario in the feature, while iterating over the feature.
    /// - Returns: The next GherkinScenario.
    public func next() -> GherkinScenario? {
        if nextIndex >= count {
            nextIndex = 0
            return nil
        } else {
            nextIndex += 1
            return scenarios[nextIndex - 1]
        }
    }

    /// Add a Scenario to the feature.
    /// - Parameter scenario: the Scenario to include in the Feature
    func append(_ scenario: GherkinScenario) {
        scenarios.append(scenario)
    }

    // MARK: - Tags

    public func canRunWithTags(onlyTagged: Set<GherkinTag>) -> Bool {
        if onlyTagged.isEmpty {
            return true
        }
        for tag in tags {
            // this is more complicated than it should have to be,
            // because using `contains` or `intersection` on Set
            // intermittently produces incorrect results.
            //    if onlyTagged.contains(tag) {
            //        return true
            //    }
            for required in onlyTagged {
                if tag == required {
                    return true
                }
            }
        }
        return false
    }
}
