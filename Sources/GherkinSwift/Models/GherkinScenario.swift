import Foundation
import XCTest

/// An individual scenario, within a feature, that can be tested.
public final class GherkinScenario: Collection, Sequence, Equatable {
    public typealias Element = GherkinAction
    public typealias Index = Int

    let file: FeatureFile
    /// The line number the Scenario starts on in the file.
    let lineNumber: Int
    var tags: Set<GherkinTag>
    var description: String
    var examples: [GherkinExample] = []

    private var actions: [GherkinAction] = []
    /// Used when iterating over the collection of actions.
    private var nextIndex = 0

    public var count: Int {
        actions.count
    }
    public var startIndex: Int {
        0
    }
    public var endIndex: Int {
        count
    }

    // MARK: - Setup

    init(file: FeatureFile, lineNumber: Int, description: String, tags: Set<GherkinTag> = []) {
        self.file = file
        self.lineNumber = lineNumber
        self.description = description
        self.tags = tags
    }

    // MARK: - Equatable

    public static func == (lhs: GherkinScenario, rhs: GherkinScenario) -> Bool {
        lhs.file == rhs.file &&
            lhs.lineNumber == rhs.lineNumber &&
            lhs.actions == rhs.actions
    }

    // MARK: - Actions Collection

    public subscript(position: Int) -> GherkinAction {
        actions[position]
    }

    public func index(after inIndex: Int) -> Int {
        inIndex + 1
    }

    /// Get the next Scenario in the feature, while iterating over the feature.
    /// - Returns: The next GherkinScenario.
    public func next() -> GherkinAction? {
        if nextIndex >= count {
            nextIndex = 0
            return nil
        }
        nextIndex += 1
        return actions[nextIndex - 1]
    }

    /// Add an action to the Scenario.
    /// - Parameters:
    ///   - text: The text to use.
    ///   - lineType: Given, When, or Then.
    ///   - lineNumber: The line number within the feature source file.
    func addAction(_ text: String, lineNumber: Int, lineType: String) {
        let action = GherkinAction(file: file, lineNumber: lineNumber, lineType: lineType, content: text)
        actions.append(action)
    }

    // MARK: - Examples Collection

    /// Add an Example to use when running the Scenario.
    /// - Parameter example: a GherkinExample
    func addExample(_ example: GherkinExample) {
        examples.append(example)
    }

    // MARK: - Tags

    public func canRunWithTags(onlyTagged: Set<GherkinTag>) -> Bool {
        if onlyTagged.isEmpty {
            return true
        }
        for tag in tags {
            for required in onlyTagged {
                if tag == required {
                    return true
                }
            }
        }
        return false
    }
}
