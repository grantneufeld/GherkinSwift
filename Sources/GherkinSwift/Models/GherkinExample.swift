import Foundation

/// A set of labels and values (a Hash/Dictionary) that constitute a single example for a Feature.
public final class GherkinExample: Equatable {
    let file: FeatureFile
    /// The line number that the example is on in the file.
    let lineNumber: Int
    /// The `label: value` pairs that make up the example’s data.
    private var exampleData: [String: String] = [:] // label: value

    init(file: FeatureFile, lineNumber: Int) {
        self.file = file
        self.lineNumber = lineNumber
    }
    // MARK: - Behave a bit like a Dictionary

    var isEmpty: Bool {
        exampleData.isEmpty
    }

    var keys: Dictionary<String, String>.Keys {
        exampleData.keys
    }

    /// Provides access to the individual `label: value` pairs in the example’s data.
    subscript(label: String) -> String? {
        get {
            exampleData[label]
        }
        set(newValue) {
            exampleData[label] = newValue
        }
    }

    // MARK: - Equatable

    public static func == (lhs: GherkinExample, rhs: GherkinExample) -> Bool {
        lhs.exampleData == rhs.exampleData
    }

    // MARK: -

    /// Get the labels and values as a comma-separated string,
    /// sorted by the label.
    /// - Returns: a String with the labels and values
    func toString() -> String {
        var results: [String] = []
        let sortedData = exampleData.sorted { first, second in
            first <= second
        }
        for (label, value) in sortedData {
            results.append("\(label): \(value)")
        }
        return results.joined(separator: ", ")
    }
}
