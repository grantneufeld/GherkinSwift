import Foundation

/// A tag applied to a Feature or Scenario in a Gherkin format file.
/// Tags are defined by lines that start with an `@` (the whole line serves as the tag).
/// Tag lines must immediately precede a Feature or Scenario line.
public struct GherkinTag: Hashable {
    let file: FeatureFile
    /// The line number that the tag is on in the file.
    let lineNumber: Int
    /// The text that defines the tag (without the leading `@`).
    let name: String

    /// Determine if two tags match.
    /// This is based solely on if they have the same name.
    /// - Parameters:
    ///   - lhs: A GherkinTag.
    ///   - rhs: Another GherkinTag.
    /// - Returns: true if both tags have the same name.
    public static func == (lhs: GherkinTag, rhs: GherkinTag) -> Bool {
        lhs.name == rhs.name
    }
}
