/// Root access point for the GherkinSwift library.
public enum GherkinSwift {
    /// Run the applicable Feature Scenarios.
    /// ⚠️ There’s still lots to do to make this actually work.
    public static func run() {
        let featuresPath = "" // ⚠️
        let reportTo = StdoutXcodeReporter()
        let features = GherkinLoader.loadFeatures(
            featuresPath: featuresPath, parserType: GherkinFileParser.self, reportTo: reportTo
        )
        let mappers = loadMappers()
        let tags: Set<GherkinTag> = []
        FeatureRunner.run(features: features, mappers: mappers, onlyTagged: tags, reportTo: reportTo)
    }

    /// Load the custom gherkin-string to function call mappers.
    public static func loadMappers() -> [GherkinMapper] {
        let mappers: [GherkinMapper] = []
        // ⚠️ how do we find and load all the custom sub-classes of GherkinMapper???
        // get the mappers directory(ies)
        // go through each file in the directory
            // if the filename ends with "Mapper.swift"
                // add it to our collection
                //    let mapperClass = ???
                //    mappers.add(mapperClass.new)
        return mappers
    }
}
