/// Errors raised while processing Gherkin format Feature files.
enum GherkinError: Error {
    case missingMapper
}
