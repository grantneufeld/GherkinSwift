import Foundation

/// Entry point for running the feature scenarios.
public struct FeatureRunner {
    /// Run each of the Scenarios in the given Features,
    /// using the methods defined by the given Mappers.
    /// Optionally limit which features and scenarios run based on any tags passed in.
    /// Results (success, failure, errors) are reported to the given MessageReporter.
    /// - Parameters:
    ///   - features: GherkinFeature Array.
    ///   - mappers: GherkinMapper Array.
    ///   - onlyTagged: GherkinTag Set where Features/Scenarios must have one of the tags to run.
    ///                 Empty set means run everything.
    ///   - reportTo: MessageReporter that will receive failures, successes, and errors.
    public static func run(
        features: [GherkinFeature],
        mappers: [GherkinMapper],
        onlyTagged: Set<GherkinTag> = [],
        reportTo: MessageReporter = StdoutXcodeReporter()
    ) {
        let runner = Self(features: features, mappers: mappers, onlyTagged: onlyTagged, reportTo: reportTo)
        runner.run()
    }

    // MARK: - Private instance attributes

    private let features: [GherkinFeature]
    private let mappers: [GherkinMapper]
    private let tags: Set<GherkinTag>
    private let report: MessageReporter

    // MARK: - Private methods for doing the work

    private init(
        features: [GherkinFeature], mappers: [GherkinMapper], onlyTagged: Set<GherkinTag>, reportTo: MessageReporter
    ) {
        self.features = features
        self.mappers = mappers
        self.tags = onlyTagged
        self.report = reportTo
    }

    /// Go through each of the features, running all applicable scenarios.
    private func run() {
        for feature in features {
            runFeature(feature: feature, onlyTagged: tags)
        }
    }

    /// Run the scenarios (and their examples) for the given Feature.
    /// - Parameters:
    ///   - feature: GherkinFeature
    ///   - onlyTagged: GherkinTag set to restrict which features/scenarios run.
    private func runFeature(feature: GherkinFeature, onlyTagged: Set<GherkinTag>) {
        var tags = onlyTagged
        if feature.canRunWithTags(onlyTagged: tags) {
            // the feature has the required tags, so no need to check Scenarios for tags
            tags = []
        }
        for scenario in feature {
            runScenarioExamples(scenario: scenario, onlyTagged: tags)
        }
    }

    private func runScenarioExamples(scenario: GherkinScenario, onlyTagged: Set<GherkinTag>) {
        let examples = scenario.examples
        if !examples.isEmpty {
            for example in examples {
                runScenario(scenario: scenario, onlyTagged: onlyTagged, example: example)
            }
        } else {
            runScenario(scenario: scenario, onlyTagged: onlyTagged)
        }
    }

    private func runScenario(scenario: GherkinScenario, onlyTagged: Set<GherkinTag>, example: GherkinExample? = nil) {
        guard scenario.canRunWithTags(onlyTagged: onlyTagged) else {
            return
        }
        var currentAction: GherkinAction?
        do {
            for action in scenario {
                currentAction = action
                try runAction(action: action)
            }
        } catch GherkinError.missingMapper {
            report.error(
                content: "missing mapper for “\(currentAction?.content ?? "<undefined>")”",
                file: currentAction?.file,
                lineNumber: currentAction?.lineNumber,
                example: example
            )
        } catch {
            fatalError("unrecognized exception thrown: '\(error)'")
        }
    }

    private func runAction(action: GherkinAction, example: GherkinExample? = nil) throws {
        let content = action.content
        let success = try GherkinMapper.handleAction(content, example: example)
        if !success {
            report.failure(
                content: "failed test: \(content)", file: action.file, lineNumber: action.lineNumber, example: example
            )
        }
    }
}
