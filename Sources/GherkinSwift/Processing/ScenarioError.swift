import Foundation

/// Details of an error encountered while processing a Scenario.
struct ScenarioError {
    /// The content of the line in the scenario where the error occurred.
    let content: String
    /// The file that the error was encountered in.
    let file: FeatureFile?
    /// The applicable line number within the feature file.
    let lineNumber: Int?
    /// The values used in an example for a scenario.
    let example: GherkinExample?
    /// Whether the error is just a warning, a test failure, or a regular error.
    let category: ReportCategory

    init(
        content: String,
        file: FeatureFile?,
        lineNumber: Int?,
        example: GherkinExample? = nil,
        category: ReportCategory = .error
    ) {
        self.content = content
        self.file = file
        self.lineNumber = lineNumber
        self.example = example
        self.category = category
    }

    var xcode: String {
        "\(file?.filePath ?? ""):\(lineNumber ?? 0):1: \(xcodeCategory): \(content)\(exampleValues)"
    }

    private var xcodeCategory: String {
        // ⚠️ how do we mark an entry as a successful test when reporting back to Xcode?
        if category == .warning {
            return "warning"
        }
        return "error"
    }

    private var exampleValues: String {
        if let example = example {
            return "; example parameters: \(example.toString())"
        }
        return ""
    }
}
