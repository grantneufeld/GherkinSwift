import Foundation

/// A collection of feature files (presumably from a file system directory).
protocol FeatureDirectory: Collection where Element == FeatureFile {
}
