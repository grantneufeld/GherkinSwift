import Foundation

/// A directory of FeatureFiles.
final class Directory: FeatureDirectory {
    typealias Element = FeatureFile

    // MARK: - Sequence Vars

    var count: Int {
        do {
            try loadFilesIfNeeded()
        } catch {
            return 0
        }
        return files.count
    }

    var startIndex: Int {
        0
    }

    var endIndex: Int {
        count
    }

    // MARK: - internal vars

    private var path: String
    private var files: [FeatureFile] = []

    // MARK: - Initialization

    init(path: String) {
        self.path = path
    }

    // MARK: - Sequence/Collection

    subscript(position: Int) -> FeatureFile {
        files[position]
    }

    func index(after index: Int) -> Int {
        index + 1
    }

    // MARK: - Files Directory Access

    /// Checks if the `files` has been populated and, if not,
    /// loads the FeatureFile data into it.
    private func loadFilesIfNeeded() throws {
        if self.files.isEmpty {
            try loadFiles()
        }
    }

    /// Set the `files` to be an Array of FeatureFile based on the
    /// directory at `path`.
    private func loadFiles() throws {
        self.files = []
        let fileNames = try FileManager.default.contentsOfDirectory(atPath: path)
        for filename in fileNames.sorted() {
            if filename.hasSuffix(".feature") {
                let filepath = "\(path)\(filename)"
                let file = FeatureFile(filePath: filepath)
                self.files.append(file)
            }
        }
    }
}
