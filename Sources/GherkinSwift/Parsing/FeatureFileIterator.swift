import Foundation

/// Iterator for FeatureFiles so they can be used in `for` loops.
/// Iterates over the lines in the file.
public struct FeatureFileIterator: IteratorProtocol {
    public let file: FeatureFile
    private let lines: [String.SubSequence]
    private var index = 0

    /// IteratorProtocol: the type of element in the collection
    public typealias Element = String

    /// Setup the iterator with the lines from the given file.
    /// - Parameter file: A FeatureFile.
    public init(file: FeatureFile) {
        self.file = file
        do {
            self.lines = try file.lines()
        } catch {
            self.lines = []
        }
    }

    /// IteratorProtocol: Get the next element.
    public mutating func next() -> String? {
        if index < lines.count {
            let line = lines[index]
            index += 1
            return String(line)
        }
        return nil
    }
}
