import Foundation

/// Record of an error encountered when dealing with a Feature file.
struct GherkinSyntaxError {
    let file: FeatureFile
    let lineNumber: Int
    let message: String
}
