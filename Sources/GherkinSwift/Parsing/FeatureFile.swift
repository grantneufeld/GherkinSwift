import Foundation

/// A Gherkin format Feature file (`.feature`).
public struct FeatureFile: Hashable, Sequence {
    public let filePath: String
    public var fileName: String {
        if let lastSlash = filePath.lastIndex(of: "/") {
            let filenameIndex = filePath.index(after: lastSlash)
            return String(filePath.suffix(from: filenameIndex))
        }
        return filePath
    }

    /// Try to get the content of the file.
    /// - Throws: ??? (stuff from String.init(contentsOfFile:)
    /// - Returns: A String of the file content.
    public func contents() throws -> String {
        try String(contentsOfFile: filePath)
    }

    /// Split the content of the file into individual lines.
    /// - Throws: ??? (stuff from String.init(contentsOfFile:)
    /// - Returns: An Array of String SubSequences.
    public func lines() throws -> [String.SubSequence] {
        let contents = try contents()
        var result = contents.split(separator: "\n", omittingEmptySubsequences: false)
        // strip trailing empty string
        let shouldBeBlank = result.popLast()
        #if DEBUG
        guard shouldBeBlank != nil && shouldBeBlank!.isEmpty else {
            fatalError("FeatureFile.lines(): last line wasn’t blank (\"\(shouldBeBlank!)\")")
        }
        #endif
        return result
    }

    // MARK: - Sequence

    /// Iterate over the lines in the file.
    /// - Returns: an Iterator
    public func makeIterator() -> FeatureFileIterator {
        FeatureFileIterator(file: self)
    }
}
