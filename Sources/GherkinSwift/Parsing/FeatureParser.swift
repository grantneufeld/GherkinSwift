import Foundation

/// Classes that can parse feature files into collections of GherkinFeatures.
public protocol FeatureParser {
    /// Collection of features produced by the parser.
    var features: [GherkinFeature] { get }

    /// Initialize a parser.
    /// - Parameters:
    ///   - file: The FeatureFile to be parsed.
    ///   - reportTo: MessageReporter that will receive messages about errors encountered while parsing.
    init(file: FeatureFile, reportTo: MessageReporter, andLimit: Int)
}
