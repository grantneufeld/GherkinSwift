import Foundation

/// Parse the feature files in the features directory.
public enum GherkinLoader {
    /// Parse each file in the features directory into a GherkinFeature.
    /// - Parameters:
    ///   - featuresPath: String of the path to the Features directory.
    ///   - parserType: The Type of FeatureParser to use for parsing.
    ///   - reportTo: The MessageReporter to post messages (errors, warnings) to.
    /// - Returns: Array of GherkinFeatures.
    public static func loadFeatures(
        featuresPath: String, parserType: FeatureParser.Type, reportTo: MessageReporter
    ) -> [GherkinFeature] {
        var features: [GherkinFeature] = []
        let featuresDirectory = Directory(path: featuresPath)
        for featureFile in featuresDirectory {
            // need to get the andLimit from somewhere,
            // rather than defaulting to zero here
            let parser = parserType.init(file: featureFile, reportTo: reportTo, andLimit: 0)
            features += parser.features
        }
        return features
    }
}
