import Foundation
@testable import GherkinSwift

enum Mocks {
    struct Report: Equatable {
        var category: String // error, warning, success, failure
        var content: String
        var file: FeatureFile?
        var lineNumber: Int?
        var example: GherkinExample?
    }

    final class Reporter: MessageReporter {
        var reports: [Report] = []
        var text: String {
            var result = ""
            for report in reports {
                result += "\(report.category):\(report.lineNumber!): \(report.content)\n"
            }
            return result
        }

        func error(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?) {
            reports.append(
                Report(category: "error", content: content, file: file, lineNumber: lineNumber, example: example)
            )
        }

        func warning(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?) {
            reports.append(
                Report(category: "warning", content: content, file: file, lineNumber: lineNumber, example: example)
            )
        }

        func success(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?) {
            reports.append(
                Report(category: "success", content: content, file: file, lineNumber: lineNumber, example: example)
            )
        }

        func failure(content: String, file: FeatureFile?, lineNumber: Int?, example: GherkinExample?) {
            reports.append(
                Report(category: "failure", content: content, file: file, lineNumber: lineNumber, example: example)
            )
        }
    }

    final class Parser: FeatureParser {
        static var initCount: Int = 0
        static var files: [FeatureFile] = []

        var features: [GherkinFeature] = []

        init(file: FeatureFile, reportTo: MessageReporter, andLimit: Int = 0) {
            Self.files.append(file)
            Self.initCount += 1
            features.append(
                GherkinFeature(
                    file: file, lineNumber: Self.initCount, title: "Mock Parser", tags: []
                )
            )
        }
    }
}
