import Foundation
@testable import GherkinSwift

/// References and convenience functions for shared resources data for tests.
enum TestHelper {
    // MARK: - Tests/GherkinSwiftTests/SampleData

    static var sampleDataPath: String {
        let src = #file
        let directoryEnd = src.lastIndex(of: "/")
        let directory = src.prefix(through: directoryEnd!)
        return "\(String(directory))SampleData/"
    }

    static let sampleDataFilenames = [
        "Empty.txt",
        "Features",
        "IncludeBlanks.txt",
        "NumberedLines.txt",
        "SimpleText.txt"
    ]

    // MARK: - Tests/GherkinSwiftTests/SampleData/Features

    static var featuresPath: String {
        "\(sampleDataPath)Features/"
    }

    static let featuresFileNames = [
        "AddToDo.feature",
        "Minimal.feature",
        "SyntaxIssues.feature"
    ]

    /// Get a FeatureFile.
    /// - Parameter filename: The filename (without suffix).
    /// - Returns: The FeatureFile for the file.
    static func featureFile(filename: String) -> FeatureFile {
        FeatureFile(filePath: "\(featuresPath)\(filename).feature")
    }
}
