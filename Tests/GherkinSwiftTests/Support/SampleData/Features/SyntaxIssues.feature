# SyntaxIssues.feature

Plain text lines can only be in specific spots.

Scenario: Without a Feature

I want an error when an I Want line doesn’t follow a Feature.
As an out-of-place As A line...
In order to illustrate an error with having an In order line out of place...

| example | table |
| out of  | place |

Feature: Gherkin Syntax Issue Testing

Scenario Outline: Actions out of order
  Then a Then line isn’t after a When or Then.
  Given the above Then line is ignored
  Then can’t follow a Given
  When the above Then line is ignored
  Given can’t follow a When
  Then will ignore the Given line above
  Given can’t follow a Then
  When also can’t follow a Then

# examples should only follow a Then
Scenario Outline: Examples out of order
Examples:
  Given there’s a given line
Examples:
  When there’s a when line
Examples:
| table is |
| out of place, too |

Scenario: Too many actions
  Given one Given
  And another Given
  And yet another Given
  But not quite too many Givens yet
  And one more Given than the 4 limit expected
  When there is a When line
  And another When
  And yet another When
  But not quite too many Whens yet
  And one more When than the 4 limit expected
  Then there is a Then
  And another Then
  And yet another Then
  But not quite too many Thens yet
  And one more Then than the 4 limit expected

Scenario: And out of place
  And there is an And out of place
  But there is also a But out of place

Scenario Outline: example with mismatched columns
  When there are examples with the wrong number of columns
  Then it should generate a syntax error
Examples:
| one | two |
| just one |
| 3 is | too | many |

Scenario Outline: Plain text lines in wrong spots.
  Given a scenario with plain text lines in the wrong spots
  (like this line)
  When the syntax is parsed
  ... while trying to follow the syntax rules
  Then there should be various errors
  like the one this line should generate.
Examples:
for example, the Examples line should be followed by plain text
| data |
and there shouldn't be plain text in the examples table
| example |
or after the table.
