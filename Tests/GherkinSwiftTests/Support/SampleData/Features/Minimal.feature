Feature: Minimal

Scenario: Do the minimum
  Given there is a minimum of content in this feature
  When the test parses this feature
  Then there should be a valid feature titled "Minimal"
