# hash-marked lines are comments, ignored by the processor
# blank lines are also ignored

@todo
Feature:
         Add To Dos
  In order to keep track of
           what I need to get done,
  As a user
       of the To Do app,
  I want to add to dos
         to my list.

Scenario Outline:
                  Add a to do when there are none
  Given there are no to dos
  When the user fills out the new to do form with <ToDo>
  Then there should be 1 to do
  And the to do <ToDo> should be showing
Examples:
| ToDo          |
| Take a nap    |
| Eat some food |

@todo
@wip
Scenario: Add a to do when
          there are some already
  Given there are 5 to dos
  When the user fills out the new to do form with "Another To Do"
  Then there should be 6 to dos
  And the to do "Another To Do" should be showing
