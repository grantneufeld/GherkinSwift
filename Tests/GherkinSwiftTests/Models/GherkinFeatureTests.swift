@testable import GherkinSwift
import XCTest

final class GherkinFeatureTests: XCTestCase {
    private let file = FeatureFile(filePath: "path/feature")
    private var factoryIndex: Int = 1

    private func scenarioFactory() -> GherkinScenario {
        let scenario = GherkinScenario(file: file, lineNumber: factoryIndex, description: "Scenario \(factoryIndex)")
        factoryIndex += 1
        return scenario
    }

    private func tagFactory(_ name: String) -> GherkinTag {
        let tag = GherkinTag(file: file, lineNumber: factoryIndex, name: name)
        factoryIndex += 1
        return tag
    }

    // MARK: - Initialization

    func testInitializationStoresParamsAndSetsDefaultsToNil() {
        let tag = GherkinTag(file: file, lineNumber: 123, name: "unittest")
        let tags: Set<GherkinTag> = [tag]
        let feature = GherkinFeature(file: file, lineNumber: 1, title: "Initialize", tags: tags)
        // parameter assignments:
        XCTAssertEqual(feature.file.filePath, "path/feature")
        XCTAssertEqual(feature.lineNumber, 1)
        XCTAssertEqual(feature.title, "Initialize")
        XCTAssertEqual(feature.tags, tags)
        // default assignments:
        XCTAssertNil(feature.inOrder)
        XCTAssertNil(feature.asA)
        XCTAssertNil(feature.iWant)
    }

    // MARK: - == Equatable

    func testEquatableAllSameValuesNoScenariosAreEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 321, title: "Same", tags: [])
        let feature2 = GherkinFeature(file: file, lineNumber: 321, title: "Same", tags: [])
        XCTAssertEqual(feature1, feature2)
    }

    func testEquatableSameValuesSameScenariosAreEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 54, title: "One", tags: [])
        let feature2 = GherkinFeature(file: file, lineNumber: 54, title: "One", tags: [])
        let scenario = scenarioFactory()
        feature1.append(scenario)
        feature2.append(scenario)
        XCTAssertEqual(feature1, feature2)
    }

    func testEquatableDiffTitlesAreNotEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 66, title: "One", tags: [])
        let feature2 = GherkinFeature(file: file, lineNumber: 66, title: "Two", tags: [])
        XCTAssertNotEqual(feature1, feature2)
    }

    func testEquatableDiffLineNumberAreNotEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 76, title: "Test", tags: [])
        let feature2 = GherkinFeature(file: file, lineNumber: 8, title: "Test", tags: [])
        XCTAssertNotEqual(feature1, feature2)
    }

    func testEquatableDiffFilesAreNotEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 9, title: "Test", tags: [])
        let otherFile = FeatureFile(filePath: "not/the/same/file")
        let feature2 = GherkinFeature(file: otherFile, lineNumber: 9, title: "Test", tags: [])
        XCTAssertNotEqual(feature1, feature2)
    }

    func testEquatableWithDifferentScenariosAreNotEqual() {
        let feature1 = GherkinFeature(file: file, lineNumber: 21, title: "Scenarios", tags: [])
        let feature2 = GherkinFeature(file: file, lineNumber: 21, title: "Scenarios", tags: [])
        feature1.append(scenarioFactory())
        feature2.append(scenarioFactory())
        XCTAssertNotEqual(feature1, feature2)
    }

    // MARK: - count

    func testCountDefaultsToZero() {
        let feature = GherkinFeature(file: file, lineNumber: 23, title: "Count", tags: [])
        XCTAssertEqual(feature.count, 0)
    }

    func testCountWithOneScenarioReturnsOne() {
        let feature = GherkinFeature(file: file, lineNumber: 34, title: "Count One", tags: [])
        feature.append(scenarioFactory())
        XCTAssertEqual(feature.count, 1)
    }

    func testCountWithMultipleScenariosReturnsTheirCount() {
        let feature = GherkinFeature(file: file, lineNumber: 45, title: "Count Multiple", tags: [])
        for _ in 1...5 {
            feature.append(scenarioFactory())
        }
        XCTAssertEqual(feature.count, 5)
    }

    // MARK: - startIndex

    func testStartIndexIsZero() {
        let feature = GherkinFeature(file: file, lineNumber: 56, title: "Start Index", tags: [])
        XCTAssertEqual(feature.startIndex, 0)
    }

    func testStartIndexWithAnyNumberOfScenariosIsAlwaysZero() {
        let feature = GherkinFeature(file: file, lineNumber: 56, title: "Start Index", tags: [])
        for _ in 1...3 {
            feature.append(scenarioFactory())
        }
        XCTAssertEqual(feature.startIndex, 0)
    }

    // MARK: - endIndex

    func testEndIndexWhenNoScenariosIsZero() {
        let feature = GherkinFeature(file: file, lineNumber: 67, title: "End Index", tags: [])
        XCTAssertEqual(feature.endIndex, 0)
    }

    func testEndIndexPointsToIndexAfterLastScenario() {
        let feature = GherkinFeature(file: file, lineNumber: 78, title: "End Index Multiple", tags: [])
        for _ in 1...4 {
            feature.append(scenarioFactory())
        }
        XCTAssertEqual(feature.endIndex, 4)
    }

    // MARK: - subscript

    // Subscripts are intended to throw a fatal error
    // if an out-of-bounds index is used.
    // So, there’s no way to test here if an empty collection
    // triggers the expected error when subscript is used,
    // or testing an index beyond the last item.

    func testSubscriptAtZeroWhenOneScenarioIsTheScenario() {
        let feature = GherkinFeature(file: file, lineNumber: 89, title: "Subscript One", tags: [])
        let scenario = scenarioFactory()
        feature.append(scenario)
        XCTAssertEqual(feature[0], scenario)
    }

    func testSubscriptWithLastValidIndexReturnsTheLastScenario() {
        let feature = GherkinFeature(file: file, lineNumber: 90, title: "Subscript Multiple", tags: [])
        let scenario = scenarioFactory()
        for _ in 1...6 {
            feature.append(scenarioFactory())
        }
        feature.append(scenario) // scenario is 7th
        XCTAssertEqual(feature[6], scenario)
    }

    // MARK: - index(after:)

    func testIndexAfterZeroReturnsOne() {
        let feature = GherkinFeature(file: file, lineNumber: 101, title: "Index Zero", tags: [])
        XCTAssertEqual(feature.index(after: 0), 1)
    }

    func testIndexAfterBigValueReturnsOneMoreThanThatValue() {
        let feature = GherkinFeature(file: file, lineNumber: 111, title: "Index Big", tags: [])
        XCTAssertEqual(feature.index(after: 1_234_567), 1_234_568)
    }

    // MARK: - next

    func testNextWhenNoScenariosReturnsNil() {
        let feature = GherkinFeature(file: file, lineNumber: 121, title: "Next", tags: [])
        XCTAssertNil(feature.next())
    }

    func testNextWhenOneScenarioReturnsScenarioThenReturnsNil() {
        let feature = GherkinFeature(file: file, lineNumber: 131, title: "Next One", tags: [])
        let scenario = scenarioFactory()
        feature.append(scenario)
        XCTAssertEqual(feature.next(), scenario)
        XCTAssertNil(feature.next())
    }

    func testNextWithMultipleScenariosCyclesThroughThenEndsWithNil() {
        let feature = GherkinFeature(file: file, lineNumber: 141, title: "Next Multiple", tags: [])
        let scenario1 = scenarioFactory()
        feature.append(scenario1)
        for _ in 1...3 {
            feature.append(scenarioFactory())
        }
        let scenarioLast = scenarioFactory()
        feature.append(scenarioLast)
        XCTAssertEqual(feature.next(), scenario1)
        for _ in 1...3 {
            _ = feature.next()
        }
        XCTAssertEqual(feature.next(), scenarioLast)
        XCTAssertNil(feature.next())
    }

    // MARK: - append

    func testAppendOnFeatureWithNoScenariosMakesScenarioFirst() {
        let feature = GherkinFeature(file: file, lineNumber: 151, title: "Append One", tags: [])
        let scenario = scenarioFactory()
        feature.append(scenario)
        XCTAssertEqual(feature.first, scenario)
    }

    func testAppendMultipleTimesKeepsTheAssignedScenariosInOrder() {
        let feature = GherkinFeature(file: file, lineNumber: 161, title: "Append Multiple", tags: [])
        let scenario1 = scenarioFactory()
        feature.append(scenario1)
        for _ in 1...4 {
            feature.append(scenarioFactory())
        }
        let scenarioLast = scenarioFactory()
        feature.append(scenarioLast)
        XCTAssertEqual(feature[0], scenario1)
        XCTAssertEqual(feature[5], scenarioLast)
    }

    // MARK: - canRunWithTags

    func testCanRunWithTagsWhenNoTags() {
        let feature = GherkinFeature(file: file, lineNumber: 171, title: "No Tags", tags: [])
        XCTAssertTrue(feature.canRunWithTags(onlyTagged: []))
    }

    func testCanRunWithTagsWhenNoRestrictionTags() {
        let feature = GherkinFeature(
            file: file, lineNumber: 181, title: "No Restriction Tags", tags: [tagFactory("tag1")]
        )
        XCTAssertTrue(feature.canRunWithTags(onlyTagged: []))
    }

    func testCanRunWithTagsIsFalseWhenMissingRequiredTag() {
        let feature = GherkinFeature(
            file: file, lineNumber: 191, title: "Missing Tag", tags: []
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("tag1")]
        XCTAssertFalse(feature.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsIsFalseWhenHasWrongTags() {
        let feature = GherkinFeature(
            file: file, lineNumber: 202, title: "Wrong Tags", tags: [tagFactory("wrong"), tagFactory("nope")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("need"), tagFactory("must")]
        XCTAssertFalse(feature.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenOneRequiredMatch() {
        let feature = GherkinFeature(
            file: file,
            lineNumber: 212,
            title: "One Match",
            tags: [tagFactory("nope"), tagFactory("yep"), tagFactory("nada")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("yep")]
        XCTAssertTrue(feature.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenOneTagOutOfManyRequiredMatches() {
        let featureTag = tagFactory("this")
        let feature = GherkinFeature(
            file: file, lineNumber: 222, title: "One of Many", tags: [featureTag]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("many"), tagFactory("this"), tagFactory("options")]
        XCTAssertTrue(feature.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenThereAreMultipleMatches() {
        let feature = GherkinFeature(
            file: file,
            lineNumber: 232,
            title: "MultipleMatches",
            tags: [tagFactory("multiple"), tagFactory("nope"), tagFactory("matches")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("other"), tagFactory("matches"), tagFactory("multiple")]
        XCTAssertTrue(feature.canRunWithTags(onlyTagged: requiredTags))
    }

    // MARK: - acts as sequence

    func testGherkinFeatureWorksAsASequence() {
        // Given a feature with a bunch of scenarios
        let feature = GherkinFeature(file: file, lineNumber: 242, title: "Sequence", tags: [])
        self.factoryIndex = 100
        for _ in 1...5 {
            feature.append(scenarioFactory())
        }
        // When we go through the feature as a sequence,
        // copying the scenario line numbers
        var result: [Int] = []
        for scenario in feature {
            result.append(scenario.lineNumber)
        }
        // Then we should have the line numbers in correct order
        XCTAssertEqual(result, [100, 101, 102, 103, 104])
    }
}
