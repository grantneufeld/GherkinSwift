@testable import GherkinSwift
import XCTest

final class GherkinScenarioTests: XCTestCase {
    private let file = FeatureFile(filePath: "path/scenario")
    private var factoryIndex: Int = 1

    private func tagFactory(_ name: String) -> GherkinTag {
        let tag = GherkinTag(file: file, lineNumber: factoryIndex, name: name)
        factoryIndex += 1
        return tag
    }

    // MARK: - Initialization

    func testInitializationStoresParamsAndSetsDefaultsToNil() {
        let tag = GherkinTag(file: file, lineNumber: 123, name: "init")
        let tags: Set<GherkinTag> = [tag]
        let scenario = GherkinScenario(file: file, lineNumber: 12, description: "Initialization", tags: tags)
        // parameter assignments:
        XCTAssertEqual(scenario.file.filePath, "path/scenario")
        XCTAssertEqual(scenario.lineNumber, 12)
        XCTAssertEqual(scenario.description, "Initialization")
        XCTAssertEqual(scenario.tags, tags)
        // default assignments:
        XCTAssertEqual(scenario.examples, [])
    }

    // MARK: - count

    func testCountDefaultsToZero() {
        let scenario = GherkinScenario(file: file, lineNumber: 23, description: "Count", tags: [])
        XCTAssertEqual(scenario.count, 0)
    }

    func testCountWithOneScenarioReturnsOne() {
        let scenario = GherkinScenario(file: file, lineNumber: 34, description: "Count One", tags: [])
        scenario.addAction("action", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario.count, 1)
    }

    func testCountWithMultipleScenariosReturnsTheirCount() {
        let scenario = GherkinScenario(file: file, lineNumber: 45, description: "Count Multiple", tags: [])
        for _ in 1...5 {
            scenario.addAction("action", lineNumber: 35, lineType: "Given")
        }
        XCTAssertEqual(scenario.count, 5)
    }

    // MARK: - startIndex

    func testStartIndexIsZero() {
        let scenario = GherkinScenario(file: file, lineNumber: 56, description: "Start Index", tags: [])
        XCTAssertEqual(scenario.startIndex, 0)
    }

    func testStartIndexWithAnyNumberOfScenariosIsAlwaysZero() {
        let scenario = GherkinScenario(file: file, lineNumber: 56, description: "Start Index", tags: [])
        for _ in 1...3 {
            scenario.addAction("action", lineNumber: 35, lineType: "Given")
        }
        XCTAssertEqual(scenario.startIndex, 0)
    }

    // MARK: - endIndex

    func testEndIndexWhenNoScenariosIsZero() {
        let scenario = GherkinScenario(file: file, lineNumber: 67, description: "End Index", tags: [])
        XCTAssertEqual(scenario.endIndex, 0)
    }

    func testEndIndexPointsToIndexAfterLastScenario() {
        let scenario = GherkinScenario(file: file, lineNumber: 78, description: "End Index Multiple", tags: [])
        for _ in 1...4 {
            scenario.addAction("action", lineNumber: 35, lineType: "Given")
        }
        XCTAssertEqual(scenario.endIndex, 4)
    }

    // MARK: - == Equatable

    func testEquatableAllSameValuesNoActionsAreEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 321, description: "Same")
        let scenario2 = GherkinScenario(file: file, lineNumber: 321, description: "Same")
        XCTAssertEqual(scenario1, scenario2)
    }

    func testEquatableSameValuesSameActionsAreEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 54, description: "One")
        let scenario2 = GherkinScenario(file: file, lineNumber: 54, description: "One")
        scenario1.addAction("the same", lineNumber: 55, lineType: "Given")
        scenario2.addAction("the same", lineNumber: 55, lineType: "Given")
        XCTAssertEqual(scenario1, scenario2)
    }

    func testEquatableDiffDescriptionsAreEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 66, description: "One")
        let scenario2 = GherkinScenario(file: file, lineNumber: 66, description: "Two")
        XCTAssertEqual(scenario1, scenario2)
    }

    func testEquatableDiffLineNumberAreNotEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 76, description: "Test")
        let scenario2 = GherkinScenario(file: file, lineNumber: 8, description: "Test")
        XCTAssertNotEqual(scenario1, scenario2)
    }

    func testEquatableDiffFileAreNotEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 9, description: "Test")
        let otherFile = FeatureFile(filePath: "not/the/same/file")
        let scenario2 = GherkinScenario(file: otherFile, lineNumber: 9, description: "Test")
        XCTAssertNotEqual(scenario1, scenario2)
    }

    func testEquatableWithDifferentActionsAreNotEqual() {
        let scenario1 = GherkinScenario(file: file, lineNumber: 21, description: "Actions")
        let scenario2 = GherkinScenario(file: file, lineNumber: 21, description: "Actions")
        scenario1.addAction("different", lineNumber: 123, lineType: "When")
        scenario2.addAction("not the same", lineNumber: 234, lineType: "Then")
        XCTAssertNotEqual(scenario1, scenario2)
    }

    // MARK: - subscript

    // Subscripts are intended to throw a fatal error
    // if an out-of-bounds index is used.
    // So, there’s no way to test here if an empty collection
    // triggers the expected error when subscript is used,
    // or testing an index beyond the last item.

    func testSubscriptAtZeroWhenOneScenarioIsTheScenario() {
        let scenario = GherkinScenario(file: file, lineNumber: 89, description: "Subscript One", tags: [])
        scenario.addAction("action", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario[0].content, "action")
    }

    func testSubscriptWithLastValidIndexReturnsTheLastScenario() {
        let scenario = GherkinScenario(file: file, lineNumber: 90, description: "Subscript Multiple", tags: [])
        for lineNumber in 1...7 {
            scenario.addAction("action", lineNumber: lineNumber, lineType: "Given")
        }
        scenario.addAction("last one", lineNumber: 35, lineType: "Given") // 8th
        XCTAssertEqual(scenario[7].content, "last one")
    }

    // MARK: - index(after:)

    func testIndexAfterZeroReturnsOne() {
        let scenario = GherkinScenario(file: file, lineNumber: 101, description: "Index Zero", tags: [])
        XCTAssertEqual(scenario.index(after: 0), 1)
    }

    func testIndexAfterBigValueReturnsOneMoreThanThatValue() {
        let scenario = GherkinScenario(file: file, lineNumber: 111, description: "Index Big", tags: [])
        XCTAssertEqual(scenario.index(after: 1_234_567), 1_234_568)
    }

    // MARK: - next

    func testNextWhenNoScenariosReturnsNil() {
        let scenario = GherkinScenario(file: file, lineNumber: 121, description: "Next", tags: [])
        XCTAssertNil(scenario.next())
    }

    func testNextWhenOneScenarioReturnsScenarioThenReturnsNil() {
        let scenario = GherkinScenario(file: file, lineNumber: 131, description: "Next One", tags: [])
        scenario.addAction("action", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario.next()?.content, "action")
        XCTAssertNil(scenario.next())
    }

    func testNextWithMultipleScenariosCyclesThroughThenEndsWithNil() {
        let scenario = GherkinScenario(file: file, lineNumber: 141, description: "Next Multiple", tags: [])
        scenario.addAction("first", lineNumber: 35, lineType: "Given")
        for _ in 1...3 {
            scenario.addAction("middle", lineNumber: 35, lineType: "Given")
        }
        scenario.addAction("last", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario.next()?.content, "first")
        for _ in 1...3 {
            _ = scenario.next()
        }
        XCTAssertEqual(scenario.next()?.content, "last")
        XCTAssertNil(scenario.next())
    }

    // MARK: - addAction

    func testAddActionOnScenarioWithNoActionsMakesActionFirst() {
        let scenario = GherkinScenario(file: file, lineNumber: 151, description: "Add One", tags: [])
        scenario.addAction("action", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario.first?.content, "action")
    }

    func testAddActionMultipleTimesKeepsTheAssignedActionsInOrder() {
        let scenario = GherkinScenario(file: file, lineNumber: 161, description: "Add Multiple", tags: [])
        scenario.addAction("one", lineNumber: 35, lineType: "Given")
        for _ in 1...4 {
            scenario.addAction("middle", lineNumber: 35, lineType: "Given")
        }
        scenario.addAction("six", lineNumber: 35, lineType: "Given")
        XCTAssertEqual(scenario[0].content, "one")
        XCTAssertEqual(scenario[5].content, "six")
    }

    // MARK: - addExample

    func testAddExampleWithNoExamplesMakesExampleFirst() {
        let scenario = GherkinScenario(file: file, lineNumber: 161, description: "Add Example", tags: [])
        let example = GherkinExample(file: file, lineNumber: 222)
        scenario.addExample(example)
        XCTAssertEqual(scenario.examples.first, example)
    }

    func testAddExampleMultipleTimesKeepsTheExamplesInOrder() {
        let scenario = GherkinScenario(file: file, lineNumber: 161, description: "Append Multiple", tags: [])
        var lineNumber = 11
        for _ in 1...5 {
            scenario.addExample(GherkinExample(file: file, lineNumber: lineNumber))
            lineNumber += 11
        }
        let lineNumbers = scenario.examples.map { example in
            example.lineNumber
        }
        XCTAssertEqual(lineNumbers, [11, 22, 33, 44, 55])
    }

    // MARK: - canRunWithTags

    func testCanRunWithTagsWhenNoTags() {
        let scenario = GherkinScenario(file: file, lineNumber: 171, description: "No Tags", tags: [])
        XCTAssertTrue(scenario.canRunWithTags(onlyTagged: []))
    }

    func testCanRunWithTagsWhenNoRestrictionTags() {
        let scenario = GherkinScenario(
            file: file, lineNumber: 181, description: "No Restriction Tags", tags: [tagFactory("tag1")]
        )
        XCTAssertTrue(scenario.canRunWithTags(onlyTagged: []))
    }

    func testCanRunWithTagsIsFalseWhenMissingRequiredTag() {
        let scenario = GherkinScenario(
            file: file, lineNumber: 191, description: "Missing Tag", tags: []
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("tag1")]
        XCTAssertFalse(scenario.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsIsFalseWhenHasWrongTags() {
        let scenario = GherkinScenario(
            file: file, lineNumber: 202, description: "Wrong Tags", tags: [tagFactory("wrong"), tagFactory("nope")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("need"), tagFactory("must")]
        XCTAssertFalse(scenario.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenOneRequiredMatch() {
        let scenario = GherkinScenario(
            file: file,
            lineNumber: 212,
            description: "One Match",
            tags: [tagFactory("nope"), tagFactory("yep"), tagFactory("nada")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("yep")]
        XCTAssertTrue(scenario.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenOneTagOutOfManyRequiredMatches() {
        let scenarioTag = tagFactory("this")
        let scenario = GherkinScenario(
            file: file, lineNumber: 222, description: "One of Many", tags: [scenarioTag]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("many"), tagFactory("this"), tagFactory("options")]
        XCTAssertTrue(scenario.canRunWithTags(onlyTagged: requiredTags))
    }

    func testCanRunWithTagsWhenThereAreMultipleMatches() {
        let scenario = GherkinScenario(
            file: file,
            lineNumber: 232,
            description: "MultipleMatches",
            tags: [tagFactory("multiple"), tagFactory("nope"), tagFactory("matches")]
        )
        let requiredTags: Set<GherkinTag> = [tagFactory("other"), tagFactory("matches"), tagFactory("multiple")]
        XCTAssertTrue(scenario.canRunWithTags(onlyTagged: requiredTags))
    }

    // MARK: - acts as sequence

    func testGherkinScenarioWorksAsASequence() {
        // Given a scenario with a bunch of scenarios
        let scenario = GherkinScenario(file: file, lineNumber: 242, description: "Sequence", tags: [])
        var lineNumber = 11
        for _ in 1...6 {
            scenario.addAction("filler", lineNumber: lineNumber, lineType: "Given")
            lineNumber += 11
        }
        // When we go through the scenario as a sequence,
        // copying the scenario line numbers
        var result: [Int] = []
        for action in scenario {
            result.append(action.lineNumber)
        }
        // Then we should have the line numbers in correct order
        XCTAssertEqual(result, [11, 22, 33, 44, 55, 66])
    }
}
