@testable import GherkinSwift
import XCTest

final class GherkinTagTests: XCTestCase {
    private let file = FeatureFile(filePath: "path/tag")

    // MARK: - Initialization

    func testInitializationStoresParams() {
        let tag = GherkinTag(file: file, lineNumber: 123, name: "init")
        XCTAssertEqual(tag.file.filePath, "path/tag")
        XCTAssertEqual(tag.lineNumber, 123)
        XCTAssertEqual(tag.name, "init")
    }

    // MARK: - == Equatable

    func testEquatableWithSameValuesAreEqual() {
        let tag1 = GherkinTag(file: file, lineNumber: 22, name: "equal")
        let tag2 = GherkinTag(file: file, lineNumber: 22, name: "equal")
        XCTAssertEqual(tag1, tag2)
    }

    func testEquatableDoesntCareAboutFileValues() {
        let tag1 = GherkinTag(file: file, lineNumber: 33, name: "equal")
        let otherFile = FeatureFile(filePath: "other/path")
        let tag2 = GherkinTag(file: otherFile, lineNumber: 33, name: "equal")
        XCTAssertEqual(tag1, tag2)
    }

    func testEquatableDoesntCareAboutLineNumbers() {
        let tag1 = GherkinTag(file: file, lineNumber: 44, name: "equal")
        let tag2 = GherkinTag(file: file, lineNumber: 45, name: "equal")
        XCTAssertEqual(tag1, tag2)
    }

    func testEquatableWithDifferentNamesAreNotEqual() {
        let tag1 = GherkinTag(file: file, lineNumber: 55, name: "diff")
        let tag2 = GherkinTag(file: file, lineNumber: 55, name: "nope")
        XCTAssertNotEqual(tag1, tag2)
    }
}
