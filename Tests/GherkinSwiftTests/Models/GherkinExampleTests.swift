@testable import GherkinSwift
import XCTest

final class GherkinExampleTests: XCTestCase {
    private let file = FeatureFile(filePath: "path/example")

    // MARK: - Initialization

    func testInitialization() {
        let example = GherkinExample(file: file, lineNumber: 234)
        XCTAssertEqual(example.file.filePath, "path/example")
        XCTAssertEqual(example.lineNumber, 234)
    }

    // MARK: - isEmpty

    func testIsEmptyDefault() {
        let example = GherkinExample(file: file, lineNumber: 345)
        XCTAssertTrue(example.isEmpty)
    }

    func testIsEmptyWithAnEmptyStringValue() {
        let example = GherkinExample(file: file, lineNumber: 45)
        example["test"] = ""
        XCTAssertFalse(example.isEmpty)
    }

    func testIsEmptyWithASingleValue() {
        let example = GherkinExample(file: file, lineNumber: 6)
        example["x"] = "y"
        XCTAssertFalse(example.isEmpty)
    }

    func testIsEmptyWithMultipleValues() {
        let example = GherkinExample(file: file, lineNumber: 7_890)
        example["a"] = "123"
        example["b"] = "456"
        example["c"] = ""
        XCTAssertFalse(example.isEmpty)
    }

    // MARK: - keys

    func testKeysDefault() {
        let example = GherkinExample(file: file, lineNumber: 8)
        let keys = example.keys
        XCTAssertTrue(keys.elementsEqual([]))
    }

    func testKeysOneEntry() {
        let example = GherkinExample(file: file, lineNumber: 99)
        example["example"] = "test"
        let keys = example.keys
        XCTAssertTrue(keys.elementsEqual(["example"]))
    }

    func testKeysMultipleEntries() {
        let example = GherkinExample(file: file, lineNumber: 100)
        example["4"] = "test"
        example["3"] = "test"
        example["2"] = "test"
        example["1"] = "test"
        let keys = example.keys.sorted().map { key in
            String(key)
        }
        XCTAssertEqual(keys, ["1", "2", "3", "4"])
    }

    // MARK: - subscript

    func testSubscriptMissing() {
        let example = GherkinExample(file: file, lineNumber: 2)
        XCTAssertNil(example["missing"])
    }

    func testSubscriptSingle() {
        let example = GherkinExample(file: file, lineNumber: 33)
        example["single"] = "1"
        XCTAssertEqual(example["single"], "1")
    }

    func testSubscriptMultiple() {
        let example = GherkinExample(file: file, lineNumber: 4)
        example["one"] = "1"
        example["two"] = "2"
        example["three"] = "3"
        XCTAssertEqual(example["one"], "1")
        XCTAssertEqual(example["two"], "2")
        XCTAssertEqual(example["three"], "3")
    }

    func testSubscriptOverwrite() {
        let example = GherkinExample(file: file, lineNumber: 55)
        example["replace"] = "initial"
        example["replace"] = "middle"
        example["replace"] = "final"
        XCTAssertEqual(example["replace"], "final")
    }

    // MARK: - == Equatable

    func testEquatableWithNoDataAreEqual() {
        let example1 = GherkinExample(file: file, lineNumber: 321)
        let example2 = GherkinExample(file: file, lineNumber: 432)
        XCTAssertEqual(example1, example2)
    }

    func testEquatableWithTheSameDataAreEqual() {
        let example1 = GherkinExample(file: file, lineNumber: 54)
        let example2 = GherkinExample(file: file, lineNumber: 654)
        example1["a"] = "1"
        example2["a"] = "1"
        example1["blah"] = "some stuff"
        example2["blah"] = "some stuff"
        XCTAssertEqual(example1, example2)
    }

    func testEquatableWithOnlyLeftSideHasDataAreNotEqual() {
        let example1 = GherkinExample(file: file, lineNumber: 76)
        let example2 = GherkinExample(file: file, lineNumber: 8)
        example1["x"] = "1234"
        example1["zzzzz"] = "more"
        XCTAssertNotEqual(example1, example2)
    }

    func testEquatableWithOnlyRightSideHasDataAreNotEqual() {
        let example1 = GherkinExample(file: file, lineNumber: 9)
        let example2 = GherkinExample(file: file, lineNumber: 109)
        example2["diff"] = "not"
        XCTAssertNotEqual(example1, example2)
    }

    func testEquatableWithDifferentDataAreNotEqual() {
        let example1 = GherkinExample(file: file, lineNumber: 21)
        let example2 = GherkinExample(file: file, lineNumber: 3)
        example1["a"] = "1"
        example1["not the same"] = "different things"
        example2["a"] = "1"
        XCTAssertNotEqual(example1, example2)
    }

    // MARK: - toString

    func testToStringDefault() {
        let example = GherkinExample(file: file, lineNumber: 654)
        XCTAssertEqual(example.toString(), "")
    }

    func testToStringEmptyStrings() {
        let example = GherkinExample(file: file, lineNumber: 654)
        example[""] = ""
        XCTAssertEqual(example.toString(), ": ")
    }

    func testToStringNormalEntry() {
        let example = GherkinExample(file: file, lineNumber: 654)
        example["normal"] = "entry"
        XCTAssertEqual(example.toString(), "normal: entry")
    }

    func testToStringTwoEntries() {
        let example = GherkinExample(file: file, lineNumber: 654)
        example["first"] = "1"
        example["second"] = "2"
        XCTAssertEqual(example.toString(), "first: 1, second: 2")
    }

    func testToStringMultipleEntries() {
        let example = GherkinExample(file: file, lineNumber: 654)
        example["1"] = "a"
        example["2"] = "b"
        example["3"] = "c"
        example["4"] = "d"
        XCTAssertEqual(example.toString(), "1: a, 2: b, 3: c, 4: d")
    }
}
