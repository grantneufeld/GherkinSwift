@testable import GherkinSwift
import XCTest

final class GherkinActionTests: XCTestCase {
    func testInitialization() {
        let file = FeatureFile(filePath: "path")
        let action = GherkinAction(file: file, lineNumber: 123, lineType: "Given", content: "this is a test")
        XCTAssertEqual(action.file.filePath, "path")
        XCTAssertEqual(action.lineNumber, 123)
        XCTAssertEqual(action.lineType, "Given")
        XCTAssertEqual(action.content, "this is a test")
    }
}
