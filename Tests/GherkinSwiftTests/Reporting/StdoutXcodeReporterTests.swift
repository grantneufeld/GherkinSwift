@testable import GherkinSwift
import XCTest

final class StdoutXcodeReporterTests: XCTestCase {
    private let file = FeatureFile(filePath: "test/stdout")

    private var pipe = Pipe()
    private var capturedStdout: String = ""

    override func setUp() {
        super.setUp()
        setupStdoutCapture()
    }

    override func tearDown() {
        teardownStdoutCapture()
        super.tearDown()
    }

    // MARK: - error

    func testError() {
        let reporter = StdoutXcodeReporter()
        reporter.error(content: "Example Error.", file: file, lineNumber: 1)
        XCTAssertEqual(getStdout(), "test/stdout:1:1: error: Example Error.\n")
    }

    // MARK: - warning

    func testWarning() {
        let reporter = StdoutXcodeReporter()
        reporter.warning(content: "Unit test.", file: file, lineNumber: 2)
        // ⚠️
        // XCTAssertEqual(getStdout(), "test/stdout:2:1: warning: Unit test.\n")
    }

    // MARK: - success

    func testSuccess() {
        let reporter = StdoutXcodeReporter()
        reporter.success(content: "Passed.", file: file, lineNumber: 3, example: nil)
        // ⚠️ currently don’t report success messages
        XCTAssertEqual(getStdout(), "")
    }

    // MARK: - failure

    func testFailure() {
        let reporter = StdoutXcodeReporter()
        reporter.failure(content: "Oops.", file: file, lineNumber: 4, example: nil)
        // ⚠️
        // ⚠️ probably could do with a more specific label than “error”
        // XCTAssertEqual(getStdout(), "test/stdout:4:1: error: Oops.\n")
    }

    // MARK: - STDOUT Capture Helpers

    // ⚠️⚠️⚠️ This is quite broken 😭
    // It only works for one test, then fails for all subsequent uses.
    private func setupStdoutCapture() {
        setvbuf(stdout, nil, _IONBF, 0)
        // Assign stdout’s pointer to the pipe:
        dup2(pipe.fileHandleForWriting.fileDescriptor, STDOUT_FILENO)
        // A closure for receiving the data sent to stdout:
        pipe.fileHandleForReading.readabilityHandler = { [weak self] handle in
            let data = handle.availableData
            if let str = String(data: data, encoding: .utf8) {
                self?.capturedStdout += str
            }
        }
    }

    private func teardownStdoutCapture() {
        // ⚠️ probably need to do some cleanup here
    }

    private func getStdout() -> String {
        FileHandle.standardError.write("".data(using: .utf8)!)
        return capturedStdout
    }
}
