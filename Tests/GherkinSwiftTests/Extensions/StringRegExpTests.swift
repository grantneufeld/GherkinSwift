@testable import GherkinSwift
import XCTest

final class StringRegExpTests: XCTestCase {
    // MARK: - length

    func testLengthEmpty() {
        XCTAssertEqual("".length, 0)
    }

    func testLengthASCII() {
        XCTAssertEqual("1234567890".length, 10)
    }

    func testLengthSpecial() {
        XCTAssertEqual("Åéîøü¡©•".length, 8)
    }

    func testLengthEmoji() {
        XCTAssertEqual("😀👨🏽‍🤝‍👨🏾".length, 14)
    }

    // MARK: - isValidRegex

    func testIsValidRegexEmpty() {
        XCTAssertFalse("".isValidRegex)
    }

    func testIsValidRegexJustCharacters() {
        XCTAssertTrue("abc123".isValidRegex)
    }

    func testIsValidRegexStart() {
        XCTAssertTrue("^start".isValidRegex)
    }

    func testIsValidRegexEnd() {
        XCTAssertTrue("end$".isValidRegex)
    }

    func testIsValidRegexSet() {
        XCTAssertTrue("[a-z0-9]".isValidRegex)
    }

    func testIsValidRegexMultiples() {
        XCTAssertTrue("A+b*".isValidRegex)
    }

    func testIsValidRegexGroups() {
        XCTAssertTrue("(abc).(123)".isValidRegex)
    }

    // MARK: - captureGroups

    func testCaptureGroupsInvalidPattern() {
        XCTAssertThrowsError(try "example".captureGroups(pattern: ""))
    }

    func testCaptureGroupsNoGroups() throws {
        let groups = try "example".captureGroups(pattern: "e")
        XCTAssertEqual(groups, [])
    }

    func testCaptureGroupsNoMatches() throws {
        let groups = try "abcdefg".captureGroups(pattern: "([0-9])")
        XCTAssertEqual(groups, [])
    }

    func testCaptureGroupsOneSimpleMatch() throws {
        let groups = try "Beep".captureGroups(pattern: "(e+)")
        XCTAssertEqual(groups, ["ee"])
    }

    func testCaptureGroupsMultipleOccurrences() throws {
        let groups = try "abcdefghi".captureGroups(pattern: "([aeiou])")
        XCTAssertEqual(groups, ["a", "e", "i"])
    }

    func testCaptureGroupsMultipleGroups() throws {
        let groups = try "About 100!".captureGroups(pattern: "^(.).* ([0-9]+).*(.)$")
        XCTAssertEqual(groups, ["A", "100", "!"])
    }

    // MARK: - gsub

    func testGsubEmptyStringsAllAround() {
        let result = "".gsub(pattern: "", with: "")
        XCTAssertEqual(result, "")
    }

    func testGsubEmptyStringButNonEmptyPattern() {
        let result = "".gsub(pattern: ".", with: "*")
        XCTAssertEqual(result, "")
    }

    func testGsubEmptyPattern() {
        let result = "abc".gsub(pattern: "", with: "*")
        XCTAssertEqual(result, "abc")
    }

    func testGsubNonMatchingPattern() {
        let result = "Test".gsub(pattern: "[0-9]", with: "*")
        XCTAssertEqual(result, "Test")
    }

    func testGsubMatchingPatternEmptyReplace() {
        let result = "Something".gsub(pattern: "[aeiou]", with: "X")
        XCTAssertEqual(result, "SXmXthXng")
    }

    func testGsubEmoji() {
        let result = "I’m 😡 here 😱!".gsub(pattern: "[😡😱]", with: "(☺️)")
        XCTAssertEqual(result, "I’m (☺️) here (☺️)!")
    }

    // MARK: - split

    func testSplitEmptyStrings() {
        let result = "".split(pattern: "")
        XCTAssertEqual(result, [""])
    }

    func testSplitEmptyPattern() {
        let result = "abc".split(pattern: "")
        XCTAssertEqual(result, ["abc"])
    }

    func testSplitEmptyStringWithPattern() {
        let result = "".split(pattern: ".")
        XCTAssertEqual(result, [""])
    }

    func testSplitNonMatchingPattern() {
        let result = "abc".split(pattern: "[0-9]")
        XCTAssertEqual(result, ["abc"])
    }

    func testSplitSimpleMatch() {
        let result = "abc,123,def".split(pattern: ",")
        XCTAssertEqual(result, ["abc", "123", "def"])
    }

    func testSplitComplexishPattern() {
        let result = "An example, with complexity!".split(pattern: "[ ,!]+")
        XCTAssertEqual(result, ["An", "example", "with", "complexity", ""])
    }
}
