@testable import GherkinSwift
import XCTest

final class DirectoryTests: XCTestCase {
    func testInvalidFilePathResultsInZeroCount() {
        let dir = Directory(path: "/invalid/path")
        XCTAssertEqual(dir.count, 0)
    }

    // MARK: - Collection

    func testCount() {
        let dir = Directory(path: TestHelper.featuresPath)
        XCTAssertEqual(dir.count, TestHelper.featuresFileNames.count)
    }

    func testCountDirectoryWithNoFeaturesIsZero() {
        let dir = Directory(path: TestHelper.sampleDataPath)
        XCTAssertEqual(dir.count, 0)
    }

    func testStartIndexIsAlwaysZero() {
        let dir = Directory(path: TestHelper.featuresPath)
        XCTAssertEqual(dir.startIndex, 0)
    }

    func testEndIndexMatchesCount() {
        let dir = Directory(path: TestHelper.featuresPath)
        XCTAssertEqual(dir.endIndex, dir.count)
    }

    func testSubscriptGetsExpectedFiles() {
        let dir = Directory(path: TestHelper.featuresPath)
        _ = dir.count // force directory to load files
        var index: Int = 0
        for filename in TestHelper.featuresFileNames {
            XCTAssertEqual(dir[index], FeatureFile(filePath: "\(TestHelper.featuresPath)\(filename)"))
            index += 1
        }
    }

    func testCollectionAllowsRelooping() {
        let dir = Directory(path: TestHelper.featuresPath)
        var result: [String] = []
        for file in dir {
            result.append(file.fileName)
        }
        XCTAssertEqual(result, TestHelper.featuresFileNames)
        result = []
        for file in dir {
            result.append(file.fileName)
        }
        XCTAssertEqual(result, TestHelper.featuresFileNames)
    }
}
