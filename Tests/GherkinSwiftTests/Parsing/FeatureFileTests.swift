@testable import GherkinSwift
import XCTest

/// This effectively incorporates the testing of FeatureFileIterator,
/// by testing Sequence compliance.
final class FeatureFileTests: XCTestCase {
    // MARK: - Initialization

    func testInitSetsTheFilePath() {
        let file = FeatureFile(filePath: "path/init")
        XCTAssertEqual(file.filePath, "path/init")
    }

    // MARK: - fileName

    func testFileNameWithEmptyPathIsEmpty() {
        let file = FeatureFile(filePath: "")
        XCTAssertEqual(file.fileName, "")
    }

    func testFileNameWithDirectoryPathIsEmpty() {
        let file = FeatureFile(filePath: "some/directory/")
        XCTAssertEqual(file.fileName, "")
    }

    func testFileNameOffRootDirIsFilename() {
        let file = FeatureFile(filePath: "/file")
        XCTAssertEqual(file.fileName, "file")
    }

    func testFileNameNoDirectoryIsFilename() {
        let file = FeatureFile(filePath: "no-directory.file")
        XCTAssertEqual(file.fileName, "no-directory.file")
    }

    func testFileNameSingleCharFilenameNoDirectoryIsFilename() {
        let file = FeatureFile(filePath: "1")
        XCTAssertEqual(file.fileName, "1")
    }

    func testFileNameSingleCharNamesIsFilename() {
        let file = FeatureFile(filePath: "a/b")
        XCTAssertEqual(file.fileName, "b")
    }

    // MARK: - contents

    func testContentsWithInvalidFilepathThrowsError() {
        let file = FeatureFile(filePath: "invalid file path")
        XCTAssertThrowsError(try file.contents())
    }

    func testContentsWithValidFilepathReturnsTheContent() throws {
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)SimpleText.txt")
        let result = try file.contents()
        XCTAssertEqual(result, "Simple text.\n")
    }

    func testContentsWithEmptyFileReturnsEmptyString() throws {
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)Empty.txt")
        let result = try file.contents()
        XCTAssertEqual(result, "")
    }

    // MARK: - lines

    func testLinesWithInvalidFilepathThrowsError() {
        let file = FeatureFile(filePath: "invalid file path")
        XCTAssertThrowsError(try file.lines())
    }

    func testLinesGetsFileContentsAsArrayOfStrings() throws {
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)NumberedLines.txt")
        let result = try file.lines()
        XCTAssertEqual(result, ["1", "2", "3", "4", "5"])
    }

    func testLinesWithEmptyFileGetsEmptyArray() throws {
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)Empty.txt")
        let result = try file.lines()
        XCTAssertEqual(result, [])
    }

    func testLinesWithBlankLinesIncludesBlanks() throws {
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)IncludeBlanks.txt")
        let result = try file.lines()
        XCTAssertEqual(result, ["", "Blanks", "", "", "Included", ""])
    }

    // MARK: - Sequence

    func testFeatureFileActsAsSequence() {
        // Given a text file with 5 lines that are just the numbers 1 through 5
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)NumberedLines.txt")
        // When we go through the lines,
        // appending the values to the result string
        var result: String = ""
        for line in file {
            result += line
        }
        // Then we should have the numbers from the lines as one string
        XCTAssertEqual(result, "12345")
    }

    func testFeatureFileActsAsSequenceWithEmptyFile() {
        // Given an empty text file
        let file = FeatureFile(filePath: "\(TestHelper.sampleDataPath)Empty.txt")
        // When we go through the lines,
        // appending the values to the result string
        var result: String = ""
        for line in file {
            result += line
        }
        // Then we should have an empty string
        XCTAssertEqual(result, "")
    }
}
