@testable import GherkinSwift
import XCTest

final class GherkinLoaderTests: XCTestCase {
    // MARK: - loadFeatures

    func testLoadFeaturesDirectoryWithNoFeatureFiles() {
        let reporter = Mocks.Reporter()
        let result = GherkinLoader.loadFeatures(
            featuresPath: TestHelper.sampleDataPath, parserType: Mocks.Parser.self, reportTo: reporter
        )
        XCTAssertEqual(result, [])
        XCTAssertEqual(Mocks.Parser.initCount, 0)
        XCTAssertEqual(Mocks.Parser.files, [])
    }

    func testLoadFeaturesFromFeaturesDirectoryMakesFeatures() {
        let reporter = Mocks.Reporter()
        let result = GherkinLoader.loadFeatures(
            featuresPath: TestHelper.featuresPath, parserType: Mocks.Parser.self, reportTo: reporter
        )
        let featureCount = TestHelper.featuresFileNames.count
        XCTAssertEqual(result.count, featureCount)
        var index: Int = 0
        for filename in TestHelper.featuresFileNames {
            let feature = result[index]
            XCTAssertEqual(feature.file.fileName, filename)
            index += 1
        }
        XCTAssertEqual(Mocks.Parser.initCount, featureCount)
        XCTAssertEqual(Mocks.Parser.files.count, featureCount)
    }
}
