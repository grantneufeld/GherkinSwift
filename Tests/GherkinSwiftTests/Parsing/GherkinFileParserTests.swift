@testable import GherkinSwift
import XCTest

// swiftlint:disable function_body_length

final class GherkinFileParserTests: XCTestCase {
    // MARK: - Minimal.feature

    func testFileParserMinimalExampleProducesFeature() {
        // Given
        let file = TestHelper.featureFile(filename: "Minimal")
        let reporter = Mocks.Reporter()
        // When
        let parser = GherkinFileParser(file: file, reportTo: reporter)
        let result = parser.features
        // Then
        let expectedFeature = GherkinFeature(file: file, lineNumber: 1, title: "Minimal", tags: [])
        let expectedScenario = GherkinScenario(file: file, lineNumber: 3, description: "Do the minimum")
        expectedScenario.addAction(
            "there is a minimum of content in this feature", lineNumber: 4, lineType: "Given"
        )
        expectedScenario.addAction(
            "the test parses this feature", lineNumber: 5, lineType: "When"
        )
        expectedScenario.addAction(
            "there should be a valid feature titled \"Minimal\"", lineNumber: 6, lineType: "Then"
        )
        expectedFeature.append(expectedScenario)
        let resultFeature = result[0]
        XCTAssertEqual(result, [expectedFeature])
        XCTAssertEqual(resultFeature.count, 1) // scenario count
        let resultScenario = resultFeature[0]
        XCTAssertEqual(resultScenario, expectedScenario)
        XCTAssertEqual(resultScenario.count, 3) // Given/When/Then actions
        XCTAssertEqual(resultScenario.examples.count, 0)
        XCTAssertEqual(reporter.reports, [])
    }

    // MARK: - AddToDo.feature

    func testFileParserAddToDoExampleProducesFeature() {
        // Given
        let file = TestHelper.featureFile(filename: "AddToDo")
        let reporter = Mocks.Reporter()
        // When
        let parser = GherkinFileParser(file: file, reportTo: reporter)
        let result = parser.features
        // Then
        let expectedFeature = GherkinFeature(
            file: file,
            lineNumber: 5,
            title: "Add To Dos",
            tags: [GherkinTag(file: file, lineNumber: 4, name: "todo")]
        )
        expectedFeature.inOrder = "to keep track of\nwhat I need to get done,"
        expectedFeature.asA = "user\n of the To Do app,"
        expectedFeature.iWant = "to add to dos\nto my list."
        // first Scenario
        let expectedScenario1 = GherkinScenario(
            file: file, lineNumber: 14, description: "Add a to do when there are none"
        )
        expectedScenario1.addAction(
            "there are no to dos", lineNumber: 16, lineType: "Given"
        )
        expectedScenario1.addAction(
            "the user fills out the new to do form with <ToDo>", lineNumber: 17, lineType: "When"
        )
        expectedScenario1.addAction(
            "there should be 1 to do", lineNumber: 18, lineType: "Then"
        )
        expectedScenario1.addAction(
            "the to do <ToDo> should be showing", lineNumber: 19, lineType: "Then"
        )
        let example1 = GherkinExample(file: file, lineNumber: 22)
        example1["ToDo"] = "Take a nap"
        expectedScenario1.addExample(example1)
        let example2 = GherkinExample(file: file, lineNumber: 23)
        example2["ToDo"] = "Eat some food"
        expectedScenario1.addExample(example2)
        expectedFeature.append(expectedScenario1)
        // second Scenario
        let expectedScenario2 = GherkinScenario(
            file: file,
            lineNumber: 27,
            description: "Add a to do when\nthere are some already",
            tags: [
                GherkinTag(file: file, lineNumber: 25, name: "todo"),
                GherkinTag(file: file, lineNumber: 26, name: "wip")
            ]
        )
        expectedScenario2.addAction(
            "there are 5 to dos", lineNumber: 29, lineType: "Given"
        )
        expectedScenario2.addAction(
            "the user fills out the new to do form with \"Another To Do\"", lineNumber: 30, lineType: "When"
        )
        expectedScenario2.addAction(
            "there should be 6 to dos", lineNumber: 31, lineType: "Then"
        )
        expectedScenario2.addAction(
            "the to do \"Another To Do\" should be showing", lineNumber: 32, lineType: "Then"
        )
        expectedFeature.append(expectedScenario2)
        // assertions
        XCTAssertEqual(result, [expectedFeature])
        let resultFeature = result[0]
        XCTAssertEqual(resultFeature.title, "Add To Dos")
        XCTAssertEqual(resultFeature.inOrder, "to keep track of\nwhat I need to get done,")
        XCTAssertEqual(resultFeature.asA, "user\nof the To Do app,")
        XCTAssertEqual(resultFeature.iWant, "to add to dos\nto my list.")
        XCTAssertEqual(resultFeature.count, 2) // scenario count
        let resultScenario1 = resultFeature[0]
        XCTAssertEqual(resultScenario1, expectedScenario1)
        XCTAssertEqual(resultScenario1.description, "Add a to do when there are none")
        XCTAssertEqual(resultScenario1.count, 4) // Given/When/Then actions
        XCTAssertEqual(resultScenario1.examples.count, 2)
        let resultScenario2 = resultFeature[1]
        XCTAssertEqual(resultScenario2, expectedScenario2)
        XCTAssertEqual(resultScenario2.description, "Add a to do when\nthere are some already")
        XCTAssertEqual(resultScenario2.count, 4) // Given/When/Then actions
        XCTAssertEqual(resultScenario2.examples.count, 0)
        XCTAssertEqual(reporter.reports, [])
    }

    // MARK: - SyntaxIssues.feature

    func testFileParserSyntaxIssuesProducesErrorReportsButNoFeatures() {
        // Given
        let file = TestHelper.featureFile(filename: "SyntaxIssues")
        let reporter = Mocks.Reporter()
        // When
        let parser = GherkinFileParser(file: file, reportTo: reporter, andLimit: 4)
        let result = parser.features
        // Then
        XCTAssertEqual(result.count, 1)
        let resultFeature = result[0]
        // scenarios still get created, although most of their content isn’t set
        XCTAssertEqual(resultFeature.count, 6)
        // there’s probably a nicer way to do this:
        let expectedReportValues = [
            ["error", 3],
            ["error", 5],
            ["error", 7],
            ["error", 8],
            ["error", 9],
            ["error", 11],
            ["error", 12],
            ["error", 17],
            ["error", 19],
            ["error", 21],
            ["error", 23],
            ["error", 24],
            ["error", 28],
            ["error", 30],
            ["error", 32],
            ["error", 33],
            ["error", 34],
            ["warning", 41],
            ["warning", 46],
            ["warning", 51],
            ["error", 54],
            ["error", 55],
            ["error", 62],
            ["error", 63],
            ["error", 67],
            ["error", 69],
            ["error", 71],
            ["error", 73],
            ["error", 75],
            ["error", 77]
        ]
        for index in 0..<expectedReportValues.count {
            let report = reporter.reports[index]
            let values = expectedReportValues[index]
            XCTAssertEqual(report.category, values[0] as! String)
            XCTAssertEqual(report.lineNumber, (values[1] as! Int))
        }
    }
}
