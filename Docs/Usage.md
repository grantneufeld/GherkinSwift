# Using GherkinSwift

You create tests to be run with GherkinSwift in two parts:

* Feature files.
* Mappers.

The feature files use the [Gherkin syntax](GherkinSyntax.md)
and are where you define test scenarios.

The mappers are Swift classes where you map the text used
in your feature files to methods to run.

Where you put the features and mappers will depend on which tool
you are using to run GherkinSwift.

## Feature Files

You define your features using [Gherkin syntax](GherkinSyntax.md),
typically in text files with names ending with `.feature`.

## Mappers

In order to be able to run the features/scenarios defined in the feature files,
there need to be mappers defined.

Mappers must be classes that inherit from the `GherkinMapper` class,
with names ending in `Mapper` (e.g., `class UserAccountMapper: GherkinMapper`).
You can have one or more of these classes.

Each must be in its own file, with the filename matching the class name
(e.g., `UserAccountMapper.swift`).

### Initialization

In the Mapper subclass, you must override the void init call (`override init() {}`)
to map text strings to custom functions to call.
This is what enables the use of plain text syntax in the Gherkin scenario actions (“Given”, “When”, “Then”)
to trigger code actions.

Within that init call, you can use calls to `given(String, method)`, `when(String, method)`,
and `then(String, method)`, to map text to specific methods you define.

You can also use `before(method)` and `after(method)`
to add methods that will be called before or after every scenario.

### Methods

The custom methods mapped by the Mappers for use in scenario actions must have the format
`(GherkinExample?) throws -> Bool` (type-aliased as `GherkinMapperMethod`).

The `GherkinExample` object that may be passed in will contain the values
from an “Examples:” table row for the scenario.
The values are accessed using the subscript operator (just like a Dictionary container).

Example:

```gherkin
| Item | Count |
| Box | 13 |
```

```swift
func myMethod(_ example: GherkinExample?) throws -> Bool
```

means that your method can call `example["Item"]` and get `"Box"`,
and `example["Count"]` will get `"13"`.

Note that all example values are strings. So, you’re responsible for any conversions to other types
(e.g., `let itemCount = Int(example["Count"])` would get `13` as an integer instead of string,
in the above example).

The `Bool` returned by the method will be ignored for methods called by “Given” or “When” actions.
It is only applicable when testing the results of a scenario in “Then” actions.
For that, `true` means the test passed, and `false` means it failed.

Throwing an error (exception) will cause the scenario to fail,
but will not halt subsequent scenarios and feature from running.

Your methods can make use of any of the calls you would normally make in a UITest `test` method.

### Example Mapper

```swift
import XCTest

/// Implementation of Gherkin mappings for MyApp.
final class MyAppMapper: GherkinMapper {
    override init() {
        super.init()
        given("I am a registered user", iAmRegistered)
    }

    func iAmRegistered(_ example: GherkinExample? = nil) throws -> Bool {
        // setup a registered user to use...
    }
}
```
