# Some Good Practices for Writing Features

Features describe the desired **behaviours** of the system, not how it is built.
They should not describe specific system implementation details—leave that for unit or integration tests.

Features should generally lean toward high-level, business domain, descriptions.

E.g., “When I login as Bob” instead of “When I click on the Login button,
And I enter "bob" in the username field, And I enter "MyPass" in the password field, …”

In general, it’s best to minimize the use of “And” (and “But”) lines when defining actions in a scenario.
Having more than a few in one scenario can be considered a code smell that may suggest
you’re either getting too close to implementation details,
or that the scenario should be split into multiple scenarios.

Features and scenarios should be written to be readily understood by non-programmer folks
who understand the business domain.
Domain specific jargon can be fine to use, but coding-specific terms should be avoided.
